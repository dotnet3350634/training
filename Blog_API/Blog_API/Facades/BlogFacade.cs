﻿using Blog_API.Data;
using Blog_API.Interfaces;
using Blog_API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Runtime.CompilerServices;

namespace Blog_API.Facades
{

    public class BlogFacade : IBlogFacade
    {
        private readonly ApplicationDbContext db;
        public BlogFacade(ApplicationDbContext injectedDB)
        {
            db = injectedDB;
        }

        public async Task<List<PostItem>> GetPostItems()
        {
            return await db.PostItems.ToListAsync();
        }

        public async Task<PostItem?> GetPostItem(int id)
        {
            return await db.PostItems.FirstOrDefaultAsync(u => u.Id == id);  
        }

        public async Task<PostItem> AddPost(PostItem pi)
        {
            await db.PostItems.AddAsync(pi);
            await db.SaveChangesAsync();
            return pi;
        }

        public async Task<PostItem> UpdatePost(int id, PostItem pi)
        {
            var post = await db.PostItems.FirstOrDefaultAsync(u => u.Id == id);
            post.Title = pi.Title;
            post.Text = pi.Text;
            await db.SaveChangesAsync();
            return post;
        }

        public async Task DeletePost(int id)
        {
            db.PostItems.Remove(db.PostItems.Single(p => p.Id == id));
            await db.SaveChangesAsync();
        }

        public async Task<List<CommentItem>> GetCommentItems()
        {
            return await db.CommentItems.ToListAsync();
        }

        public async Task<List<CommentItem>> GetCommentItemsForPost(int id)
        {
            return await db.CommentItems.Where(u => u.PostId == id).ToListAsync();
        }

        public async Task<CommentItem> AddComment(CommentItem ci)
        {
            await db.CommentItems.AddAsync(ci);
            await db.SaveChangesAsync();
            return ci;
        }

        public async Task<CommentItem> UpdateComment(int id, CommentItem ci)
        {
            var comment = await db.CommentItems.FirstOrDefaultAsync(u => u.Id == id);
            comment.PostId = ci.PostId;
            comment.Title = ci.Title;
            comment.Text = ci.Text;
            await db.SaveChangesAsync();
            return comment;
        }

        public async Task DeleteComment(int id)
        {
            db.CommentItems.Remove(db.CommentItems.Single(c => c.Id == id));
            await db.SaveChangesAsync();
        }

    }

}
