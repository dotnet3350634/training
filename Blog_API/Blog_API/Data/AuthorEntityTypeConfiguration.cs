﻿using Blog_API.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace Blog_API.Data
{
    public class AuthorEntityTypeConfiguration : IEntityTypeConfiguration<Author>
    {
        public void Configure(EntityTypeBuilder<Author> builder)
        {
            builder
                .HasKey(e => e.Id);
            builder
                .Property(e => e.Id)
                .HasColumnName("Id")
                .HasColumnType("int")
                .IsRequired();
            builder
                .HasMany(e => e.PostItems)
                .WithMany(e => e.Authors)
                .UsingEntity<PostAuthor>();
            builder
               .Property(e => e.FirstName)
               .HasColumnName("FirstName")
               .HasColumnType("nvarchar")
               .HasMaxLength(50)
               .IsRequired();
            builder
               .Property(e => e.LastName)
               .HasColumnName("LastName")
               .HasColumnType("nvarchar")
               .HasMaxLength(50)
               .IsRequired();

        }

    }
}