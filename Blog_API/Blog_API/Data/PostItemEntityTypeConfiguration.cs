﻿using Blog_API.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Blog_API.Data
{
    public class PostItemEntityTypeConfiguration : IEntityTypeConfiguration<PostItem>
    {
        public void Configure(EntityTypeBuilder<PostItem> builder)
        {
            builder
                .HasKey(e => e.Id);
            builder
                .HasMany(e => e.CommentItems)
                .WithOne(e => e.PostItem)
                .HasForeignKey(e => e.PostId)
                .IsRequired();
            builder
                .HasMany(e => e.Authors)
                .WithMany(e => e.PostItems)
                .UsingEntity<PostAuthor>(
                    l => l.HasOne<Author>(e => e.Author).WithMany(e => e.PostAuthors).HasForeignKey(e => e.AuthorId),
                    r => r.HasOne<PostItem>(e => e.PostItem).WithMany(e => e.PostAuthors).HasForeignKey(e => e.PostId));
            builder
                .Property(e => e.Id)
                .HasColumnName("Id")
                .HasColumnType("int")
                .IsRequired();
            builder
                .Property(e => e.Title)
                .HasColumnName("Title")
                .HasColumnType("nvarchar")
                .HasMaxLength(50)
                .IsRequired();
            builder
                .Property(e => e.Text)
                .HasColumnName("Text")
                .HasColumnType("nvarchar")
                .HasMaxLength(4000)
                .IsRequired();
        }
    }
}
