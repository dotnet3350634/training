﻿using Blog_API.Models;
using Microsoft.EntityFrameworkCore;

namespace Blog_API.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        #region Required
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            new PostItemEntityTypeConfiguration().Configure(modelBuilder.Entity<PostItem>());
            new CommentItemEntityTypeConfiguration().Configure(modelBuilder.Entity<CommentItem>());
            new AuthorEntityTypeConfiguration().Configure(modelBuilder.Entity<Author>());

            // Data seed
            // Post Items
            modelBuilder.Entity<PostItem>().HasData(new PostItem { Id = 1, Title = "fdfdsf1", Text = "1kfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfd" });
            modelBuilder.Entity<PostItem>().HasData(new PostItem { Id = 2, Title = "fdfds2", Text = "2kfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfd" });
            modelBuilder.Entity<PostItem>().HasData(new PostItem { Id = 3, Title = "fdfdsf3", Text = "3kfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfd" });
            modelBuilder.Entity<PostItem>().HasData(new PostItem { Id = 4, Title = "fdfdsf4", Text = "4kfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfd" });
            modelBuilder.Entity<PostItem>().HasData(new PostItem { Id = 5, Title = "fdfdsf5", Text = "5kfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfd" });
            // Comment Items
            // For post 1
            modelBuilder.Entity<CommentItem>().HasData(new CommentItem { Id = 1, PostId = 1, Title = "comment1", Text = "comment 1 in post 1" });
            modelBuilder.Entity<CommentItem>().HasData(new CommentItem { Id = 2, PostId = 1, Title = "comment2", Text = "comment 2 in post 1" });
            // Fror post 2
            modelBuilder.Entity<CommentItem>().HasData(new CommentItem { Id = 3, PostId = 2, Title = "comment3", Text = "comment 3 in post 2" });
            modelBuilder.Entity<CommentItem>().HasData(new CommentItem { Id = 4, PostId = 2, Title = "comment4", Text = "comment 4 in post 2" });
            // For post 3
            modelBuilder.Entity<CommentItem>().HasData(new CommentItem { Id = 5, PostId = 3, Title = "comment5", Text = "comment 5 in post 3" });
            modelBuilder.Entity<CommentItem>().HasData(new CommentItem { Id = 6, PostId = 3, Title = "comment6", Text = "comment 6 in post 3" });
            // For post 4
            modelBuilder.Entity<CommentItem>().HasData(new CommentItem { Id = 7, PostId = 4, Title = "comment7", Text = "comment 7 in post 4" });
            modelBuilder.Entity<CommentItem>().HasData(new CommentItem { Id = 8, PostId = 4, Title = "comment8", Text = "comment 8 in post 4" });
            // For post 5
            modelBuilder.Entity<CommentItem>().HasData(new CommentItem { Id = 9, PostId = 5, Title = "comment9", Text = "comment 9 in post 5" });
            modelBuilder.Entity<CommentItem>().HasData(new CommentItem { Id = 10, PostId = 5, Title = "comment10", Text = "comment 10 in post 5" });

            // Authors
            modelBuilder.Entity<Author>().HasData(new Author { Id = 1, FirstName = "Prokop", LastName = "Tunel" });
            modelBuilder.Entity<Author>().HasData(new Author { Id = 2, FirstName = "Tomáš", LastName = "Fuk" });
            modelBuilder.Entity<Author>().HasData(new Author { Id = 3, FirstName = "Jan", LastName = "Novák" });
            modelBuilder.Entity<Author>().HasData(new Author { Id = 4, FirstName = "Adam", LastName = "Vyskočil" });
            modelBuilder.Entity<Author>().HasData(new Author { Id = 5, FirstName = "Filip", LastName = "Kolzub" });
            // Assign author to posts
            modelBuilder.Entity<PostAuthor>().HasData(new PostAuthor { AuthorId = 1, PostId = 1, Participation = 20});
            modelBuilder.Entity<PostAuthor>().HasData(new PostAuthor { AuthorId = 2, PostId = 1, Participation = 50 });
            modelBuilder.Entity<PostAuthor>().HasData(new PostAuthor { AuthorId = 3, PostId = 1, Participation = 30 });

            modelBuilder.Entity<PostAuthor>().HasData(new PostAuthor { AuthorId = 4, PostId = 2, Participation = 50 });
            modelBuilder.Entity<PostAuthor>().HasData(new PostAuthor { AuthorId = 5, PostId = 2, Participation = 50 });

            modelBuilder.Entity<PostAuthor>().HasData(new PostAuthor { AuthorId = 3, PostId = 3, Participation = 100 });

            modelBuilder.Entity<PostAuthor>().HasData(new PostAuthor { AuthorId = 3, PostId = 4, Participation = 10 });
            modelBuilder.Entity<PostAuthor>().HasData(new PostAuthor { AuthorId = 1, PostId = 4, Participation = 90 });

        }

        #endregion

        public DbSet<PostItem> PostItems { get; set; } = null!;
        public DbSet<CommentItem> CommentItems { get; set; } = null!;
    }
}
