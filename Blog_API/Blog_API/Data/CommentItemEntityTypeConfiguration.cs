﻿using Blog_API.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Blog_API.Data
{
    public class CommentItemEntityTypeConfiguration : IEntityTypeConfiguration<CommentItem>
    {
        public void Configure(EntityTypeBuilder<CommentItem> builder)
        {
            builder
                .HasKey(x => x.Id);
            builder
               .Property(x => x.Id)
               .HasColumnName("Id")
               .HasColumnType("int")
               .IsRequired();
            builder
                .HasIndex(x => x.PostId);
            builder
                .HasOne(e => e.PostItem)
                .WithMany(e => e.CommentItems)
                .HasForeignKey(e => e.PostId)
                .IsRequired();
            builder
                .Property(p => p.PostId)
                .HasColumnName("PostId")
                .HasColumnType("int")
                .IsRequired();
            builder
                .Property(p => p.Title)
                .HasColumnName("Title")
                .HasColumnType("nvarchar")
                .HasMaxLength(30)
                .IsRequired();
            builder
                .Property(p => p.Text)
                .HasColumnName("Text")
                .HasColumnType("nvarchar")
                .HasMaxLength(500)
                .IsRequired();
        }
    }
}
