﻿namespace Blog_API.Models
{
    public class Author
    {
       public int Id { get; set; }
       public string FirstName { get; set; }
       public string LastName { get; set; }

        public List<PostItem> PostItems { get; } = new();
        public List<PostAuthor> PostAuthors { get; set; } = new();
    }
}