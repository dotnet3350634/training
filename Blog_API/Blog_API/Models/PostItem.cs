﻿using System.ComponentModel.DataAnnotations;

namespace Blog_API.Models
{
    public class PostItem
    {

        public int Id { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public ICollection<CommentItem> CommentItems { get; } = new List<CommentItem>();

        public List<Author> Authors { get; } = new();
        public List<PostAuthor> PostAuthors { get; } = new();
    }
}