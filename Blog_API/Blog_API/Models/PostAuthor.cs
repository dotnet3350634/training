﻿namespace Blog_API.Models
{
    public class PostAuthor
    {
       public int PostId { get; set; }
       public int AuthorId { get; set; }
       public int Participation { get; set; }

       public PostItem PostItem { get; set; } = null!;
       public Author Author { get; set; } = null!;
    }
}
