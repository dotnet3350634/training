﻿using System.ComponentModel.DataAnnotations;

namespace Blog_API.Models
{
    public class CommentItem
    {
        public int Id { get; set; }
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public PostItem PostItem { get; set; } = null!;
    }
}
