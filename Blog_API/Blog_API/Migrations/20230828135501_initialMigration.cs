﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Blog_API.Migrations
{
    /// <inheritdoc />
    public partial class initialMigration : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Author",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Author", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PostItems",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Text = table.Column<string>(type: "nvarchar(4000)", maxLength: 4000, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostItems", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CommentItems",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PostId = table.Column<int>(type: "int", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    Text = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommentItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CommentItems_PostItems_PostId",
                        column: x => x.PostId,
                        principalTable: "PostItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PostAuthor",
                columns: table => new
                {
                    PostId = table.Column<int>(type: "int", nullable: false),
                    AuthorId = table.Column<int>(type: "int", nullable: false),
                    Participation = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostAuthor", x => new { x.AuthorId, x.PostId });
                    table.ForeignKey(
                        name: "FK_PostAuthor_Author_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Author",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PostAuthor_PostItems_PostId",
                        column: x => x.PostId,
                        principalTable: "PostItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Author",
                columns: new[] { "Id", "FirstName", "LastName" },
                values: new object[,]
                {
                    { 1, "Prokop", "Tunel" },
                    { 2, "Tomáš", "Fuk" },
                    { 3, "Jan", "Novák" },
                    { 4, "Adam", "Vyskočil" },
                    { 5, "Filip", "Kolzub" }
                });

            migrationBuilder.InsertData(
                table: "PostItems",
                columns: new[] { "Id", "Text", "Title" },
                values: new object[,]
                {
                    { 1, "1kfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfd", "fdfdsf1" },
                    { 2, "2kfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfd", "fdfds2" },
                    { 3, "3kfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfd", "fdfdsf3" },
                    { 4, "4kfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfd", "fdfdsf4" },
                    { 5, "5kfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfdkfkdajfkldafjdsfd", "fdfdsf5" }
                });

            migrationBuilder.InsertData(
                table: "CommentItems",
                columns: new[] { "Id", "PostId", "Text", "Title" },
                values: new object[,]
                {
                    { 1, 1, "comment 1 in post 1", "comment1" },
                    { 2, 1, "comment 2 in post 1", "comment2" },
                    { 3, 2, "comment 3 in post 2", "comment3" },
                    { 4, 2, "comment 4 in post 2", "comment4" },
                    { 5, 3, "comment 5 in post 3", "comment5" },
                    { 6, 3, "comment 6 in post 3", "comment6" },
                    { 7, 4, "comment 7 in post 4", "comment7" },
                    { 8, 4, "comment 8 in post 4", "comment8" },
                    { 9, 5, "comment 9 in post 5", "comment9" },
                    { 10, 5, "comment 10 in post 5", "comment10" }
                });

            migrationBuilder.InsertData(
                table: "PostAuthor",
                columns: new[] { "AuthorId", "PostId", "Participation" },
                values: new object[,]
                {
                    { 1, 1, 20 },
                    { 1, 4, 90 },
                    { 2, 1, 50 },
                    { 3, 1, 30 },
                    { 3, 3, 100 },
                    { 3, 4, 10 },
                    { 4, 2, 50 },
                    { 5, 2, 50 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_CommentItems_PostId",
                table: "CommentItems",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_PostAuthor_PostId",
                table: "PostAuthor",
                column: "PostId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CommentItems");

            migrationBuilder.DropTable(
                name: "PostAuthor");

            migrationBuilder.DropTable(
                name: "Author");

            migrationBuilder.DropTable(
                name: "PostItems");
        }
    }
}
