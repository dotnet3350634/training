﻿using Blog_API.Interfaces;
using Blog_API.Models;
using Microsoft.AspNetCore.Mvc;

namespace Blog_API.Controllers
{

    [Route("api/v1/blog")]
    [ApiController]
    public class BlogController : ControllerBase
    {
        private readonly IBlogFacade blogFacade;
        public BlogController(IBlogFacade facade)
        {
            blogFacade = facade;
        }

        // POSTS
        [HttpGet("readAllPosts")]
        public async Task<IActionResult> ReadPostItems()
        {
            return Ok(await blogFacade.GetPostItems());
        }

        [HttpGet("readPost/{id:int}")]
        public async Task<IActionResult> ReadPostItem(int id)
        {
            var result = await blogFacade.GetPostItem(id);
            if (result == null)
            {
                return NotFound();
            }
            return Ok();
        }

        [HttpPost("addPost")]
        public async Task<IActionResult> AddPostItem(PostItem pi)
        {
            return Ok(await blogFacade.AddPost(pi));
        }

        [HttpPut("updatePost/{id:int}")]
        public async Task<IActionResult> UpdatePost(int id, PostItem pi)
        {
            return Ok(await blogFacade.UpdatePost(id, pi));
        }

        [HttpDelete("deletePost{id:int}")]
        public async Task<IActionResult> DeletePost(int id)
        {
            await blogFacade.DeletePost(id);
            return Ok();
        }

        // COMMENTS
        [HttpGet("allComments")]
        public async Task<IActionResult> GetComments()
        {
            return Ok(await blogFacade.GetCommentItems());
        }

        [HttpGet("allCommentsForPost/{id:int}")]
        public async Task<IActionResult> GetComment(int id)
        {
            return Ok(await blogFacade.GetCommentItemsForPost(id));
        }

        [HttpPost("addComment")]
        public async Task<IActionResult> AddCommentItem(CommentItem ci)
        {
            return Ok(blogFacade.AddComment(ci));
        }

        [HttpPut("updateComment/{id:int}")]
        public async Task<IActionResult> UpdateComment(int id, CommentItem ci)
        {
            return Ok(await blogFacade.UpdateComment(id, ci));
        }

        [HttpDelete("deleteComment{id:int}")]
        public async Task<IActionResult> DeleteComment(int id)
        {
            await blogFacade.DeleteComment(id);
            return Ok();
        }
    }
}
