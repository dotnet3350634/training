﻿using Blog_API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Blog_API.Interfaces
{
    public interface IBlogFacade
    {
        public Task<List<PostItem>> GetPostItems();
        public Task<PostItem?> GetPostItem(int id);
        public Task<PostItem> AddPost(PostItem pi);
        public Task<PostItem> UpdatePost(int id, PostItem pi);
        public Task DeletePost(int id);
        public Task<List<CommentItem>> GetCommentItems();
        public Task<List<CommentItem>> GetCommentItemsForPost(int id);
        public Task<CommentItem> AddComment(CommentItem ci);
        public Task<CommentItem> UpdateComment(int id, CommentItem pi);
        public Task DeleteComment(int id);
    }
}
